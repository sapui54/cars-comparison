/* global QUnit */

sap.ui.require(["pwsproject/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
