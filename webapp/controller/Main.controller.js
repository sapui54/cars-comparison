sap.ui.define([
    "sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageBox) {
        "use strict";

        return Controller.extend("pwsproject.controller.Main", {
            onInit: function () {

            },
            onCompare: function (oEvent) {
                const selectedCars = this.getView().byId("cars").getSelectedItems();
                if (selectedCars.length < 2) {
                    const message = selectedCars.length > 0 ? "1 car." : "no cars.";
                    MessageBox.error("You have selected " + message + "\n Select at least 2 cars.");
                }
                else {
                    const carsToCompareModel = this.getView().getModel("carsToCompare");
                    const carsToCompare = selectedCars.map(car => this.getView().getModel().getProperty(car.getBindingContextPath()));
                    carsToCompareModel.setData(carsToCompare);
                    const oRouter = this.getOwnerComponent().getRouter();
                    oRouter.navTo("Compare");
                }
            }
        });
    });